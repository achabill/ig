import { Component } from "@angular/core";
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './components/login.component.html'
})
export class LoginComponent {
    public error: string = "";
    loading: boolean = false;
    constructor(public loginService: LoginService, public router: Router) { }

    login(username: string, password: string) {
        var self = this;
        self.loading = true;
        this.loginService.login(username, password).then(res => {
            if (res.error) {
                self.loading = false;
                self.error = res.error.message;
            } else {
                self.loading = false;
                console.log('navigate');
                this.router.navigate(['/dashboard']);
            }
        })

    }
}