import { Component } from "@angular/core";
import { LoginService } from '../services/login.service';
import { Observable } from "rxjs/Observable";
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Http } from '@angular/http';

let config = require('../config.json');
let endpoint = "http://localhost:" + config.server.port + "/" + config.server.base + "/thread";

@Component({
    selector: 'dashboard',
    templateUrl: './components/dashboard.component.html'
})
export class DashboardComponent {
    feeds: any[];
    currentFeed = null;
    threads: any[];
    feedsloading: boolean = false;
    threadsloading: boolean = false;
    uid: string;
    constructor(public loginService: LoginService, public db: AngularFireDatabase, public http: Http) {
        var self = this;
        self.feedsloading = true;

        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                self.uid = user.uid;
                var ref = firebase.database().ref("feeds/" + user.uid);
                ref.on('value', function (snapshot) {
                    self.feeds = [];
                    snapshot.forEach(function (childSnapshot) {
                        var childKey = childSnapshot.key;
                        var val = childSnapshot.val();
                        var feed = {
                            fullName: val.igFeed.accounts[0].fullName,
                            picture: val.igFeed.accounts[0].picture,
                            text: val.igFeed.items[0].text,
                            timestamp: getDate(parseInt(val.igFeed.items[0].timestamp)),
                            id: val.igFeed.id,
                            label: val.label,
                            accountId: val.igFeed.accounts[0].id,
                            firebaseKey: childKey
                        };
                        self.feedsloading = false;
                        self.feeds.push(feed);
                    });
                });
            } else {

            }
        });
    };

    updateLabel(label) {
        var self = this;
        self.getFeedById(self.currentFeed.id).then(snapshot => {
            var key = snapshot.key;
            var val = snapshot.val();
            val.label = label;
            firebase.database().ref("feeds/" + self.uid + "/" + key).update(val).then(x => {
                self.currentFeed.label = label;
            })
        })
    }

    getFeedById(id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var ref = firebase.database().ref("feeds/" + self.uid);
            ref.once("value", function (snapshot) {
                snapshot.forEach(childSnapshot => {
                    var val = childSnapshot.val();
                    console.log(val);
                    if (val.igFeed.id == id) {
                        resolve(childSnapshot);
                    }
                })
            })
        })
    }

    setCurrentFeedItem(feed) {
        var self = this;
        self.currentFeed = feed;
        let username = self.loginService.data.user.displayName;
        let token = self.loginService.data.token;
        let id = feed.id;
        let _endpoint = endpoint + "?username=" + username + "&token=" + token + "&id=" + id;

        self.http.get(_endpoint).subscribe(res => {
            let _res = res.json();
            self.threads = [];
            _res.forEach(t => {
                let feedAccountId = self.currentFeed.accountId;
                var side = "";
                var picture = "";
                if (t.accountId == feedAccountId) {
                    side = "left";
                    picture = self.currentFeed.picture;
                } else {
                    side = "right";
                    picture = self.currentFeed.picture;
                }
                var text = t.text;
                var timestamp = getChatTime(parseInt(t.timestamp));

                var thread = {
                    side: side,
                    picture: picture,
                    text: text,
                    timestamp: timestamp
                };
                self.threads.push(thread);
            });
        })
    };
}
var getDate = function (d) {
    var c = Date.now();
    var diffMs = (d - c); // milliseconds between now & Christmas
    var diffDays = Math.floor(diffMs / 86400000); // days
    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutesf()

    if (diffDays > 7) {
        return new Date(d).toLocaleString()
    } else if (diffDays < 7 && diffDays > 2) {
        return diffDays + " days ago";
    } else if (diffDays == 1) {
        return "yesterday";
    } else if (diffHrs > 12) {
        return new Date(d).getHours() + ":" + new Date(d).getMinutes();
    } else if (diffHrs > 1 && diffHrs < 12) {
        return diffHrs + " hrs ago";
    } else {
        return diffMins + " mins ago"
    }
}
var getChatTime = function (d) {
    //return new Date(d).getHours() + ":" + new Date(d).getMinutes();
    return new Date(d).toLocaleString();
}