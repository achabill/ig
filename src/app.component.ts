import { NgModule, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuthModule } from 'angularfire2/auth';
import * as firebase from 'firebase';


/**
 * Libraries
 */
import { AngularFireModule } from 'angularfire2';


@Component({
  selector: 'App',
  templateUrl: './app.component.html'
})
export class AppComponent {
}

const options = require('./config.json');

firebase.initializeApp({
  "apiKey": "AIzaSyA6GcvMSujVVXtEXfUsBljM6RtBW3v2dV8",
  "authDomain": "instagram-messenger-2c153.firebaseapp.com",
  "databaseURL": "https://instagram-messenger-2c153.firebaseio.com/",
  "projectId": "instagram-messenger-2c153",
  "storageBucket": "gs://instagram-messenger-2c153.appspot.com"
});

import { routes } from './app.routes';
import { LoginComponent } from './components/login.component';
import { DashboardComponent } from './components/dashboard.component';
import { LoginService } from './services/login.service';

// Initialize Firebase
export const firebaseConfig = {
  apiKey: options.firebase.apiKey,
  authDomain: options.firebase.authDomain,
  databaseURL: options.firebase.databaseURL,
  projectId: options.firebase.projectId,
  storageBucket: options.firebase.storageBucket,
};

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp({
      "apiKey": "AIzaSyA6GcvMSujVVXtEXfUsBljM6RtBW3v2dV8",
      "authDomain": "instagram-messenger-2c153.firebaseapp.com",
      "databaseURL": "https://instagram-messenger-2c153.firebaseio.com/",
      "projectId": "instagram-messenger-2c153",
      "storageBucket": "gs://instagram-messenger-2c153.appspot.com"
    }),
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  declarations: [AppComponent, LoginComponent, DashboardComponent],
  bootstrap: [AppComponent],
  providers: [LoginService, { provide: APP_BASE_HREF, useValue: '/' }, AngularFireDatabase]
})
export class AppModule { }