import 'rxjs/add/operator/map';
import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import * as firebase from 'firebase';
let config = require('../config.json');


@Injectable()
export class LoginService {

    public data: any;
    constructor(public http: Http) { }
    endoint: string = "http://localhost:" + config.server.port + "/" + config.server.base + "/login";
    login(username: string, password: string) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.http.post(self.endoint, {
                username: username,
                password: password
            }).subscribe(res => {
                var _res = res.json();
                if (!_res.error) {
                    self.data = _res;
                    self.data.user.password = password;
                    firebase.auth().signInWithEmailAndPassword(username + "@igmessenger.com", password).then(user => {
                        console.log(user);
                    }).catch(function (error) {
                        resolve(error)
                    });
                }
                resolve(_res);
            });
        });
    }
}